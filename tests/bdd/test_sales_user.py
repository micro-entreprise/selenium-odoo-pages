"""Sale's user workflow feature tests."""
import pytest
from pytest_bdd import given, parsers, scenario, then, when

from sop.mailhog import pdfs_from_email, wait_email
from sop.odoo.sales import SaleOrderFormViewPage, SaleOrderListViewPage

pytestmark = pytest.mark.usefixtures("selenium")


@scenario("feature/sales_user.feature", "I can see only my onw sales")
def test_i_can_see_only_my_onw_sales():
    """I can see only my onw sales."""


@scenario("feature/sales_user.feature", "I confirm a quotation")
def test_i_confirm_a_quotation():
    """I confirm a quotation."""


@scenario("feature/sales_user.feature", "I send quotation by email")
def test_i_send_quotation_by_email():
    """I send quotation by email."""


@given("I send the quotation to the customer by email")
@when("I send the quotation to the customer by email")
def given_i_send_the_quotation_to_the_customer_by_email(frontend):
    """I send the quotation to the customer by email."""
    sale_order_form_view = SaleOrderFormViewPage(frontend)
    sale_order_form_view.send_quotation()


@when("I filter to my own sales")
def i_filter_to_my_own_sales(frontend):
    """I filter to my own sales."""
    sale_order_list = SaleOrderListViewPage(frontend)
    sale_order_list.defined_filter("My Quotations")


@then(
    parsers.parse(
        'I can see quotation has been send with "{expected_attachment_count:d}" attached document'
    )
)
def i_can_see_quotation_has_been_send_with_1_attached_document(
    customer_name, expected_attachment_count
):
    """I can see quotation has been send with "1" attached document."""
    expected_email_count = 1
    emails = wait_email(
        "to",
        f"{customer_name}@test-bdd.org",
        expected_total=expected_email_count,
        timeout=45,
    )
    attachments = pdfs_from_email(emails.get("items", [{}])[0])
    assert (
        len(attachments) == expected_attachment_count
    ), "Email received with wrong number of pdf attachments"


@then("the number of records is still the same")
def the_number_of_records_is_still_the_same(clicked_menu_page, count_record):
    """the number of records is still the same."""
    assert int(clicked_menu_page.total_record) == count_record


@then("the quotation has been transform to a sale order")
def the_quotation_has_been_transform_to_a_sale_order(frontend):
    """the quotation has been transform to a sale order."""
    sale_order_form_view = SaleOrderFormViewPage(frontend)
    assert sale_order_form_view.state_el.value == "sale"
