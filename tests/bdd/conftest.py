import os
from uuid import uuid4

import pytest
from pytest_bdd import given, parsers, when

from sop.odoo import OdooBasePage, odoo_authentication
from sop.odoo.sales import SaleOrderFormViewPage, SaleOrderListViewPage
from sop.testing.conftest import *  # noqa

ODOO_URL = os.environ.get("ODOO_URL", "http://127.0.0.1:9608")


@pytest.fixture()
def anonymous_session(webdriver):
    webdriver.get(ODOO_URL)
    return webdriver


@pytest.fixture()
def frontend(anonymous_session):
    """An alias to anonymous_session."""
    return anonymous_session


@given(parsers.parse('I log as "{login}" user'))
def i_log_as_user_role(frontend, login):
    odoo_authentication(frontend, login, login)


@given(
    parsers.parse('I go to the "{page_name}" page'),
    target_fixture="clicked_menu_page",
)
def clicked_menu_page(frontend, page_name):
    """I go to the "quotation" page."""
    MENU = {
        "quotations": ["click_menu_quotations"],
        "create product": ["click_menu_product_template", "click_create"],
    }
    page = OdooBasePage(frontend)
    for menu in MENU.get(page_name):
        page = getattr(page, menu)()
    return page


@given("I remove all filters")
def i_remove_all_filters(clicked_menu_page):
    """I remove all filters."""
    clicked_menu_page.clear_filters()
    return clicked_menu_page


@given(
    "the number of records",
    target_fixture="count_record",
)
def count_record(clicked_menu_page):
    """the number of records."""
    return clicked_menu_page.total_record


@given("I create a draft quotation", target_fixture="customer_name")
def i_create_a_draft_quotation(frontend):
    """I create a draft quotation."""
    sale_order_form_view = SaleOrderListViewPage(frontend).click_create()
    customer_name = str(uuid4())
    partner_dialog = sale_order_form_view.partner_el.create_and_update(customer_name)
    partner_dialog.email_el = f"{customer_name}@test-bdd.org"
    partner_dialog.save()
    line = sale_order_form_view.order_line_el.new()
    line.product_el.click_item("Drawer Black")
    line.quantity_el = 25
    sale_order_form_view.click_save()
    return customer_name


@given("I confirm the quotation")
@when("I confirm the quotation")
def i_confirm_the_quotation(frontend):
    """I confirm the quotation."""
    sale_order_form_view = SaleOrderFormViewPage(frontend)
    sale_order_form_view.confirm()
