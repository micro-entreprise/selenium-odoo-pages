Feature: Sale's user workflow

  As Marc demo
  I want to manage my own sales
  So that I can transform quotation to sales
  without caring about others sales

  Background:
    Given I log as "demo" user
    And I go to the "quotations" page

  Scenario: I can see only my onw sales
    Given I remove all filters
    And the number of records
    When I filter to my own sales
    Then the number of records is still the same

  Scenario: I send quotation by email
    Given I create a draft quotation
    When I send the quotation to the customer by email
    Then I can see quotation has been send with "1" attached document

  Scenario: I confirm a quotation
    Given I create a draft quotation
    And I send the quotation to the customer by email
    When I confirm the quotation
    Then the quotation has been transform to a sale order
