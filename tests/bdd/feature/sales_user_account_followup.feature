Feature: Invoice and payment workflow

  As Marc demo
  I can generate a sale ready to invoice by
  admin user.
  I can follow the invoice and payment state
  fom the sale order.


  Background:
    Given I log as "demo" user
    And I go to the "quotations" page
    And I create a draft quotation
    And I confirm the quotation
    And I log as "admin" user
    And I go to the "quotations" page
    And I remove all filters
    And I search and open the sale order view
    And I generate and confirm the invoice

  Scenario: I make sure sale order has been invoiced
    Given I log as "demo" user
    And I go to the "quotations" page
    And I remove all filters
    When I search and open the sale order view
    Then the order is marked as "invoiced"

  Scenario: I make sure sale order invoice has been paid
    Given I register the payment
    And I log as "demo" user
    And I go to the "quotations" page
    And I remove all filters
    When I search and open the sale order view
    Then the order is marked as "paid"
