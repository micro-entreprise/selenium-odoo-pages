"""Invoice and payment workflow feature tests."""
import pytest
from pytest_bdd import given, scenario, then, when

from sop.odoo.accounting import InvoiceFormViewPage
from sop.odoo.sales import SaleOrderFormViewPage, SaleOrderListViewPage

pytestmark = pytest.mark.usefixtures("selenium")


@scenario(
    "feature/sales_user_account_followup.feature",
    "I make sure sale order has been invoiced",
)
def test_i_make_sure_sale_order_has_been_invoiced():
    """I make sure sale order has been invoiced."""


@scenario(
    "feature/sales_user_account_followup.feature",
    "I make sure sale order invoice has been paid",
)
def test_i_make_sure_sale_order_invoice_has_been_paid():
    """I make sure sale order invoice has been paid."""


@given("I generate and confirm the invoice")
def i_generate_and_confirm_the_invoice(frontend):
    """I generate and confirm the invoice."""
    sale_order_form = SaleOrderFormViewPage(frontend)
    wizard = sale_order_form.create_invoice_wizard()
    wizard.method_el = "delivered"
    # To improve this example we could remove billing group
    # to demo user let him generate the invoice but could not
    # post invoice nor register payments
    invoice_page = wizard.create_and_open_invoice()
    invoice_page.confirm()
    # instantiate account invoice form to wait page load
    return InvoiceFormViewPage(frontend)


@given("I search and open the sale order view")
@when("I search and open the sale order view")
def i_go_to_the_generated_sale_order_by_customer(frontend, customer_name):
    """I go to the generated sale order by customer."""
    sale_order_list = SaleOrderListViewPage(frontend)
    sale_order_list.add_filter(customer_name)
    sale_order_form = sale_order_list.open_form_view(customer_name, timeout=8)
    return sale_order_form


@given("I register the payment")
def i_register_the_payment(frontend):
    """I register the payment."""
    invoice_form = InvoiceFormViewPage(frontend)
    wizard = invoice_form.register_payement()
    wizard.create_payment()
    # re-instantiate account invoice form to wait page loaded
    return InvoiceFormViewPage(frontend)


@then('the order is marked as "invoiced"')
def the_order_is_marked_as_invoiced(frontend):
    sale_order_form = SaleOrderFormViewPage(frontend)
    sale_order_form.click_stat_button("action_view_invoice")
    invoice_form = InvoiceFormViewPage(frontend)
    assert invoice_form.state_el.value == "posted"


@then('the order is marked as "paid"')
def the_order_is_marked_as_paid(frontend):
    """the order is marked as paid."""
    sale_order_form = SaleOrderFormViewPage(frontend)
    sale_order_form.click_stat_button("action_view_invoice")
    invoice_form = InvoiceFormViewPage(frontend)
    invoice_form.ribbon_el.assertTextEqual("PAID")
