FROM python:3.8


WORKDIR /usr/sop/
COPY requirements.* ./
RUN pip install -r requirements.txt -r requirements.tests.txt -r requirements.dev.txt

COPY . .
RUN pip install -e .
ENTRYPOINT [ "py.test", "-s", "--reruns", "1" ]
CMD ["-v", "-n", "2", "tests/"]
