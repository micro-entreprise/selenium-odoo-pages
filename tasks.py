from time import sleep

from invoke import task


@task
def lint(c):
    """Linting code"""
    c.run("pre-commit install --install-hooks")
    c.run("pre-commit run --all-files --show-diff-on-failure")


@task
def build(c):
    """Build python package"""
    c.run("rm -rf dist/")
    c.run("python setup.py sdist bdist_wheel")
    c.run("twine check dist/*")
    c.run("pip install dist/*.whl")
    c.run("pip install -e .")


@task(post=[build])
def release(c, part):
    """Bump version part (major.minor.patch) and build.
    (Do not commit automaticaly)."""
    c.run(f"bump2version {part}")


@task
def push_on_pypi(c):
    """push to any wheel house (TODO)"""
    c.run("twine upload dist/*")


@task
def setup_odoo(c):
    """Start odoo and initialize with
    sale_managment and account modules"""
    c.run("docker-compose up -d")
    wait_odoo(c)
    c.run(
        'docker-compose exec -T odoo bash -c "odoo --no-http --stop-after-init -i account,sale_management"'
    )
    c.run("docker-compose restart odoo")
    c.run(
        'docker-compose exec -T db psql -U odoo -d odoo -c "'
        "INSERT INTO ir_mail_server(name, smtp_host, smtp_port, smtp_user, smtp_pass, smtp_encryption, active) "
        "VALUES ('fake smtp', 'smtp.local', 1025, 'test', 'test', 'none', TRUE);"
        '"'
    )


@task
def init_test(c):
    """Install test dependencies"""
    c.run("pip install -r requirements.tests.txt")
    c.run("pip install -e .")


@task(pre=[init_test])
def init_dev(c):
    """Init dev env with dev dependencies"""
    c.run("pip install -r requirements.dev.txt")
    c.run("pre-commit install --install-hooks")


@task
def init_docker_test(c):
    c.run("docker-compose build integration-test")


@task(pre=[init_docker_test, setup_odoo])
def test(c):
    """launch test"""
    wait_smtpfake(c)
    wait_odoo(c)
    wait_selenium(c)
    c.run("docker-compose run integration-test")


@task(pre=[init_test, setup_odoo])
def test_local(c):
    """launch test"""
    wait_smtpfake(c)
    wait_odoo(c)
    wait_selenium(c)
    c.run("pytest --reruns 1 -v -n 2")


def wait(c, host, port, service, timeout=30):
    c.run(
        "docker run --rm "
        "-v ${PWD}/wait-for-it.sh:/tmp/wait-for-it.sh "
        f"--network seleniumodoopages_internal "
        "python:3 "
        f"/tmp/wait-for-it.sh {host}:{port} -s -t {timeout} -- echo {service} is ready "
        "|| (docker-compose "
        f"logs --tail 100 ${service};"
        f'echo "{service} wasn\'t ready in the given time"; exit 1)'
    )


def wait_database(c):
    wait(c, "db", 5432, "Postgresql")


def wait_odoo(c):
    wait(c, "odoo", 8069, "odoo")


def wait_smtpfake(c):
    wait(c, "smtp.local", 8025, "smtpfake")


def wait_selenium(c):
    wait(c, "selenium-hub", 4444, "selenium-hub")
    sleep(2)
    c.run(
        """
        docker-compose exec -T \
            selenium-hub /opt/bin/check-grid.sh --host selenium-hub --port 4444 \
            || (docker-compose logs \
                    --tail 100 selenium-hub firefox chrome ; \
                echo "selenium status wasn't ready (green)"; exit 1)
    """
    )
